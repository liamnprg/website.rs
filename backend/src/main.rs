#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate rand;
extern crate ring;
extern crate hex;

pub mod schema;
pub mod models;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
use self::models::{NewLogin,Login};
use ring::digest;
use rand::thread_rng;
use rand::Rng;
use std::str::from_utf8;
use hex::encode;

//TODO: Make a function for adding, deleting, and updating user profiles. Stop allowing multiple
//people of the same name to exist. Make sure password in database isn't a plaintext.
pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub fn new_login<'a>(conn: &PgConnection, uname: &'a str, password: &'a str) -> Login {
    use schema::logins;

    
    let mut rng = thread_rng();
    let salt = &format!("{}",rng.gen_range(u64::min_value(),u64::max_value()));
    let hash = encode(chash(salt.to_string(),password.to_string()).as_ref());
    let new_login = NewLogin {
        username: uname,
        password: &hash,
        salt: salt,
    };

    println!("{:?}", new_login);
    diesel::insert_into(logins::table)
        .values(&new_login)
        .get_result(conn)
        .expect("Error saving new post")
}

pub fn chash(salt: String, password: String) -> digest::Digest {
    digest::digest(&digest::SHA384, &(format!("{}{}",salt,password)).into_bytes())
}

pub fn gandc(connection: &PgConnection, uname: String, password2check: String) -> bool {
    use schema::logins::dsl::*;
    //this gets the username out of the db
    let res = logins.filter(username.eq(uname.clone()))
        .limit(1)
        .load::<Login>(connection)
        .expect("SUPER ERROR: UNABLE TO GET DATABASE ENTRIES!!!!!!!");

    //for every login that matches <username>
    for login in res {
        //leave audit trail
        println!("Checking auth for {}", uname.clone());

        //calculate the hash
        let h = chash(login.clone().salt,password2check.clone());

        //remember to hex encode our sha384 output from the hasher
        //compare with db entry
        if encode(h).as_bytes() == login.password.as_bytes() {
            return true;
        }
    }
    false
}

fn main() {
//change passowrd in database from raw password to hash
    let connection = establish_connection();
//    new_login(&connection, "jeff","kaplan");
    println!("{}",gandc(&connection,"jeff".to_string(),"kaplan".to_string()));
    /*
    let results = logins.filter(username.eq("jeff"))
        .limit(5)
        .load::<Login>(&connection)
        .expect("Error loading posts");

    println!("Displaying {} posts", results.len());
    for login in results {
        println!("----USERNAME------\n");
        println!("{}", login.username);
        println!("---PASSWORD-------\n");
        println!("{}", login.password);
        println!("---SALT-------\n");
        println!("{}", login.salt);
        println!("---HASHED-------\n");
        println!("{:?}", chash(login.salt,login.password).as_ref());
    }*/
}
