
pub use super::schema::logins;

#[derive(Insertable,Debug)]
#[table_name="logins"]
pub struct NewLogin<'a> {
    pub password: &'a str,
    pub username: &'a str,
    pub salt: &'a str,
}

#[derive(Queryable,Debug,Clone)]
pub struct Login {
    pub id: i32,
    pub username: String,
    pub password: String,
    pub salt: String,
}

